package com.rapidminer.emm.qualityMeasures;


import java.util.Iterator;

import com.rapidminer.emm.tools.Subgroup;
import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.ConditionedExampleSet;
import com.rapidminer.example.set.NoMissingAttributesCondition;

public class KruskalCorrelation implements QualityMeasure{


	@Override
	public double evaluate(Subgroup aSubgroup, ExampleSet theData,Attribute targetOne, Attribute targetTwo) {
		ExampleSet e = extract(theData, targetOne, targetTwo); // reduced example set
		long c = 0; // concordant pairs
		long d = 0; // discordant pairs
		int n = 0; // number of times iterator i is bumped
		Iterator<Example> i = e.iterator();
		while (i.hasNext()) {
			// iterate through all possible pairs
			Example z1 = i.next();
			n++;
			double x = z1.getValue(targetOne);
			double y = z1.getValue(targetTwo);
            if (targetTwo.isNominal() && targetOne != null) {
                String yString = targetTwo.getMapping().mapIndex((int)y);
                y = targetOne.getMapping().getIndex(yString);
            }
			Iterator<Example> j = e.iterator();
			for (int k = 0; k < n; k++)
				j.next(); // increment j to match i
			while (j.hasNext()) {
				// move on to subsequent examples
				Example z2 = j.next();
				double xx = z2.getValue(targetOne);
				double yy = z2.getValue(targetTwo);
                if (targetTwo.isNominal() && targetOne != null) {
                    String yyString = targetTwo.getMapping().mapIndex((int)yy);
                    yy = targetOne.getMapping().getIndex(yyString);
                }

				if ((x > xx && y > yy) || (x < xx && y < yy))
					c++;
				// concordant pair
				else
					d++; // discordant pair
			}
		}
		double num = c - d;
		double den = c + d;
		if (den != 0)
			return num / den;
		else
			return 0;
	
	
	}

	@Override
	public double getPValue(double correlationValue) {
		// TODO Auto-generated method stub
		return 0;
	}

	private static ExampleSet extract(ExampleSet eSet, Attribute a, Attribute b) {
		// create a new example set containing just attributes a and b
		ExampleSet e = (ExampleSet) eSet.clone();
		e.getAttributes().clearRegular();
		e.getAttributes().clearSpecial();
		e.getAttributes().addRegular(a);
		e.getAttributes().addRegular(b);
		return new ConditionedExampleSet(e, new NoMissingAttributesCondition(e, null));
	}
}
