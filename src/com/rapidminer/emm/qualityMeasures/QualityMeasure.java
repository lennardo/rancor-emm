package com.rapidminer.emm.qualityMeasures;

import com.rapidminer.emm.tools.Subgroup;
import com.rapidminer.example.Attribute;
import com.rapidminer.example.ExampleSet;

public interface QualityMeasure {

	
	//public double evaluate(Subgroup aSubgroup, ExampleSet theData);
	
	public double evaluate(Subgroup aSubgroup, ExampleSet theData, Attribute targetOne, Attribute targetTwo);
	public double getPValue(double correlationValue);

}
