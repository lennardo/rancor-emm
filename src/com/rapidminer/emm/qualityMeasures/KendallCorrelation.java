package com.rapidminer.emm.qualityMeasures;


import com.rapidminer.emm.tools.NormalDistribution;
import com.rapidminer.emm.tools.Subgroup;
import com.rapidminer.example.Attribute;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.AttributeValueFilter;
import com.rapidminer.example.set.Condition;
import com.rapidminer.example.set.ConditionedExampleSet;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.performance.RankStatistics;

public class KendallCorrelation implements QualityMeasure{


	private ExampleSet base, sample;
	private double comCorr;


	@Override
	public double evaluate(Subgroup aSubgroup, ExampleSet theData,Attribute t1, Attribute t2) {


		base = theData;
		Condition allConditions = new AttributeValueFilter(theData, aSubgroup.conditionsToString());
		ExampleSet workingSet = new ConditionedExampleSet(theData, allConditions);
		ExampleSet complementSet = new ConditionedExampleSet(theData,allConditions,true);
		sample = workingSet;
		aSubgroup.setCoverage(sample.size());

		double tau = 0;
		try {
			tau = RankStatistics.tau_b(workingSet, t1, t2);
		} catch (OperatorException e) {
			e.printStackTrace();
		}

		try {
			comCorr =  RankStatistics.tau_b(complementSet, t1, t2);
			aSubgroup.setComplementCorrelationValue(comCorr);
		} catch (OperatorException e) {
			e.printStackTrace();
		}


		return tau;

	}
	@Override
	public double getPValue(double correlationValue){
		int aSize = sample.size();
		int aComplementSize = base.size() - sample.size();
		if (aSize <= 2 || aComplementSize <= 2) // either sample is too small
			return Double.NaN;

		NormalDistribution aNormalDistro = new NormalDistribution(0.0,1.0);
		double aComplementSampleSize = base.size() - sample.size();
		double aSubgroupVariance = 0.437 / (sample.size() - 4);
		double aComplementVariance = 0.437 / (aComplementSampleSize - 4);
		double aZScore = (transform2FisherScore(correlationValue) - transform2FisherScore(comCorr))
						 / Math.sqrt(aSubgroupVariance+aComplementVariance);

		//[example:] z = (obs - mean)/std = (0.9730 - 0.693)/0.333 = 0.841
		double anErfValue = aNormalDistro.calcErf(aZScore/Math.sqrt(2.0));
		double aPValue = 0.5*(1.0 + anErfValue);
		//System.out.println(aPValue);
		return  (aPValue>0.5) ?  2*(1.0-aPValue): 2*aPValue ; //
	}


	private double transform2FisherScore(double theCorrelationValue)
	{
		//z' = 0.5 ln[(1+r)/(1-r)]
		return 0.5 * Math.log((1+theCorrelationValue)/(1-theCorrelationValue));
	}

}
