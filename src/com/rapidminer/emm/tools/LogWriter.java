package com.rapidminer.emm.tools;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


public class LogWriter {
	
	private String filepath = "Subgroups";
	
	public void testLog(){
				
		this.clearLog();
				
		this.writeLineToLog("test line");
				
	}
	
	
	public LogWriter(String filepath){
		this.filepath = filepath;
	}
	
	/**
	 * 
	 */
	public void clearLog(){
				
		try{
			
			File file = new File(filepath);
			
			if(file.exists()){
				
				file.delete();
				
			}
			
			file.createNewFile();
			
		}
		catch(IOException ioe){
			
			ioe.printStackTrace();
			
		}
		
	}
	public void writeLineToLog(String line, String filepath){
		this.filepath=filepath;
		writeLineToLog(line);
	}
	/**
	 * @param line
	 */
	public void writeLineToLog(String line){
		
		File file = new File(filepath);
		
		BufferedWriter bufferedWriter = null;

		try {
			
			bufferedWriter = new BufferedWriter(new FileWriter(file, true));
						
		    bufferedWriter.write(line);
		    
		    bufferedWriter.newLine();
		    
		    bufferedWriter.flush();
		} 
		catch (IOException ex) {
			
			ex.printStackTrace();
			
		} 
		finally {
			
		   try {
			   
			   bufferedWriter.close();
			   
		   } 
		   catch (IOException ex) {
			   
			   ex.printStackTrace();
			   
		   }
		   
		}
		
	}

}
