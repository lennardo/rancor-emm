package com.rapidminer.emm.tools;

import java.util.List;

import com.rapidminer.example.ExampleSet;

public interface RefinementOperator {
	
	public List<Subgroup> refine(Subgroup aSubgroup, ExampleSet theData);

}
