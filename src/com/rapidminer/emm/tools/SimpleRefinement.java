package com.rapidminer.emm.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.AttributeValueFilter;
import com.rapidminer.example.set.Condition;
import com.rapidminer.example.set.ConditionedExampleSet;

public class SimpleRefinement implements RefinementOperator{

	private int NUMBER_OF_BINS;
	private double fuzzyValue;
	private ExampleSet parent;
	
	public SimpleRefinement(int numberOfBins, double f){
		NUMBER_OF_BINS = numberOfBins;
		fuzzyValue = f;
	}
	
	
	@Override
	public List<Subgroup> refine(Subgroup aSubgroup, ExampleSet theData) {
		parent = theData;
		List<Subgroup> newSubgroups = new ArrayList<Subgroup>();
	
		Attributes attributes = theData.getAttributes();
		Iterator<Attribute> itAtt = attributes.allAttributes();
		Attribute currentAttribute;//use, get conditionsasSinglestring
		ExampleSet subgroupData;
		
		if(aSubgroup.conditionsToString().equals("empty")){
			subgroupData = theData; //starting with empty description
		}else{
			subgroupData= new ConditionedExampleSet(theData,  new AttributeValueFilter(theData, aSubgroup.conditionsToString()));
		}
		 
		
		while(itAtt.hasNext()){
			
			currentAttribute = itAtt.next();
			
			//Helper
			ConditionList theNewConditions; 

			//Numeric refinment
			if(currentAttribute.isNumerical()){
				if(!booleanDomain(subgroupData, currentAttribute)){
				
				double[] allValues = getNumericDomain(subgroupData, currentAttribute);
				double[] splitPoints= new double[NUMBER_OF_BINS-1];
				
				for(int j=0; j < splitPoints.length;j++){
					splitPoints[j] = allValues[allValues.length*(j+1)/(NUMBER_OF_BINS)];
				}
				//Helper references
				EMMCondition newLeqCondition, newGeqCondition;
				//System.out.println("\n the splitpoints are:");
				
				/*for(double splitPoint: splitPoints){
					System.out.print(splitPoint+" , ");
				}*/
				for(double splitPoint: splitPoints){
					
					//LEQ
					newLeqCondition = new EMMCondition(currentAttribute, 0, String.valueOf(splitPoint),fuzzyValue);
					theNewConditions = aSubgroup.getConditions();
					//Check if condition is already present
					if(theNewConditions.addA(newLeqCondition)){
						newSubgroups.add(new Subgroup(theNewConditions));						
					}
					//GEQ
					newGeqCondition = new EMMCondition(currentAttribute, 1, String.valueOf(splitPoint),fuzzyValue);
					theNewConditions = aSubgroup.getConditions();
					//Check if condition is already present
					if(theNewConditions.addA(newGeqCondition)){						
						newSubgroups.add(new Subgroup(theNewConditions));							
					}	
				}
				}else{
					
					//Boolean case hack
					EMMCondition newFCondition, newTCondition;
					newFCondition = new EMMCondition(currentAttribute,4,"0",fuzzyValue);
					newTCondition = new EMMCondition(currentAttribute,4,"1",fuzzyValue);
					
					theNewConditions = aSubgroup.getConditions();
					
					if(theNewConditions.addA(newFCondition)){
						newSubgroups.add(new Subgroup(theNewConditions));
					}
					theNewConditions = aSubgroup.getConditions();
					if(theNewConditions.addA(newTCondition)){
						newSubgroups.add(new Subgroup(theNewConditions));
					}
				}
				
				
			}else if(currentAttribute.isNominal()){
				String[] nominalValues = getUniqueNominalDomain(theData, currentAttribute);
				/*System.out.println("Nominal Values: ");
				for(String s: nominalValues){
					System.out.print(s+" ");
				}*/
				
				EMMCondition newEQCondition, newNEQCondition;
				for(String nom: nominalValues){
					
					//EQ
					newEQCondition = new EMMCondition(currentAttribute, 4, nom);
					theNewConditions = aSubgroup.getConditions();
					//Check if condition is already present
					if(theNewConditions.addA(newEQCondition)){		
						newSubgroups.add(new Subgroup(theNewConditions));						
					}
					
					//NEQ
					newNEQCondition = new EMMCondition(currentAttribute, 2, nom);
					theNewConditions = aSubgroup.getConditions();
					//Check if condition is already present
					if(theNewConditions.addA(newNEQCondition)){
						newSubgroups.add(new Subgroup(theNewConditions));						
					}
				}
			}
		}
		return newSubgroups;
	}
	
	//Returns all Numeric Values of anAttribute in the given ExampleSet, sorted!
	public double[] getNumericDomain(ExampleSet exampleSet, Attribute anAttribute)
	{
		 //TODO optimize

		// First create TreeSet<Float>, then copy to float[], not ideal,
		// but I lack the inspiration to write a RB-tree for floats
		Set<Double> allValues = new TreeSet<Double>();
		for (Example example: exampleSet){
			allValues.add(example.getNumericalValue(anAttribute));
		}
		
		//HOWTO get the average Value
		//exampleSet.getStatistics(anAttribute, "average");
		double[] aResult = new double[allValues.size()];
		int i = -1;
		for (double f : allValues)
			aResult[++i] = f;

		Arrays.sort(aResult);
		
		return aResult;
	}
	
	private boolean booleanDomain(ExampleSet exampleSet, Attribute anAttribute){
		double[] test = getUniqueNumericDomain(exampleSet, anAttribute);
		if(test.length == 2){
			if(test[0]==0 && test[1]==1){
				return true;
			}else{
				return false;
			}
		}else{			
			return false;
		}
	}
	
	public double[] getUniqueNumericDomain(ExampleSet exampleSet, Attribute anAttribute)
	{
		 //TODO optimize

		// First create TreeSet<Float>, then copy to float[], not ideal,
		// but I lack the inspiration to write a RB-tree for floats
		Set<Double> allValues = new TreeSet<Double>();
		for (Example example: exampleSet){
			if(!allValues.contains(example.getNumericalValue(anAttribute))){
				allValues.add(example.getNumericalValue(anAttribute));				
			}
		}

		
		//HOWTO get the average Value
		//exampleSet.getStatistics(anAttribute, "average");
		double[] aResult = new double[allValues.size()];
		int i = -1;
		for (double f : allValues)
			aResult[++i] = f;

		Arrays.sort(aResult);
		
		return aResult;
	}
	
	public double[] getNumericDomain(ExampleSet exampleSet, int[] selectionMask, Attribute anAttribute)
	{
		 //TODO optimize

		// First create TreeSet<Float>, then copy to float[], not ideal,
		// but I lack the inspiration to write a RB-tree for floats
		Set<Double> allValues = new TreeSet<Double>();
		for (Example example: exampleSet){
			allValues.add(example.getNumericalValue(anAttribute));
		}
		
		//HOWTO get the average Value
		//exampleSet.getStatistics(anAttribute, "average");
		double[] aResult = new double[allValues.size()];
		int i = -1;
		for (double f : allValues)
			aResult[++i] = f;

		Arrays.sort(aResult);
		
		return aResult;
	}
	
    
private int[] calculateMapping(Condition condition, boolean inverted) {
	List<Integer> indices = new LinkedList<Integer>();
	
	// create mapping
	int exampleCounter = 0;
	for (Example example : parent) {
        if (!inverted) {
            if (condition.conditionOk(example))
                indices.add(exampleCounter);
        } else {
            if (!condition.conditionOk(example))
                indices.add(exampleCounter);                
        }
		exampleCounter++;
	}
	
	int[] mapping = new int[indices.size()];
	int m = 0;
	for (int index : indices) {
		mapping[m++] = index;
	}
	return mapping;
}
	
	//Returns a List of all unique Nominal Values of an Attribute in a given ExampleSet
	public String[] getUniqueNominalDomain(ExampleSet exampleSet, Attribute anAttribute){

		//TODO optimize
		Set<String> allNominalValues = new TreeSet<String>();
		for(Example example: exampleSet){
			if(!allNominalValues.contains(example.getNominalValue(anAttribute)))
				allNominalValues.add(example.getNominalValue(anAttribute));
		}
		String[] aResult = new String[allNominalValues.size()];
		int i = -1;
		for(String s: allNominalValues){
			aResult[++i] = s;
		}

		return aResult;
	}

}
