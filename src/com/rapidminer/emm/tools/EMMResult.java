package com.rapidminer.emm.tools;

import java.text.DecimalFormat;
import java.util.List;

import com.rapidminer.operator.SimpleResultObject;

public class EMMResult extends SimpleResultObject{

	private static final long serialVersionUID = 247292665996620169L;
	private List<Subgroup> theListOfSubgroups;
	private String q;
	private String correlationCoefficient = "r";
	
	DecimalFormat df = new DecimalFormat("#.##");
	public EMMResult(List<Subgroup> aSubgroupList, int resultNumber, String correlationType) {
		
		super("Overview","" );
		
		theListOfSubgroups = aSubgroupList;
		
		q=String.valueOf(resultNumber);
		
		//setting the correct name of calculated coefficient
		switch(correlationType){
		case "Pearson":
			correlationCoefficient = "r";
			break;
		case "Spearman":
			correlationCoefficient = "rho";
			break;
		case "Kendall":
			correlationCoefficient = "tau_b";
			break;
		default:
			break;
		}

	}
	
	@Override
	//Generating a string to present the output
	public String toString(){
		
		String res= q+"-best found subgroups: ";
		for(int i = theListOfSubgroups.size()-1,j = 1; i >=0;i-- ){
			res+="\n\n"+ (j++) +".: " + theListOfSubgroups.get(i).conditionsToString() 
					+"\t coverage "+ theListOfSubgroups.get(i).getCoverage()
						+"\n\t phi: "+ theListOfSubgroups.get(i).getMeasureValue() + "  "
							+ correlationCoefficient+": "+theListOfSubgroups.get(i).getCorrelationValue()
							+ " Complement Correlation: "+ theListOfSubgroups.get(i).getComplementCorrelationValue()
							+ " P-Value: "+theListOfSubgroups.get(i).getPValue();
		}
		
		return res;
	}
}
