package com.rapidminer.emm.tools;

/**CORTANA CODE**/

import java.util.*;

import com.rapidminer.example.set.Condition;

public class ConditionList extends ArrayList<EMMCondition> implements Comparable<ConditionList>
{

	private static final long serialVersionUID = -6064467825410499064L;

	public ConditionList() {}

	public ConditionList copy()
	{
	
		ConditionList aNewConditionList = new ConditionList();
		for(EMMCondition aCondition : this){
			aNewConditionList.addCondition(aCondition.copy());
		}

		return aNewConditionList;

	}
	
	public boolean addA(EMMCondition aCondition){
		int test;
		for(int i = 0; i<this.size();i++){
			test = this.get(i).compa(aCondition);
			
			switch(test){
				case -2:
					remove(this.get(i));
					add(aCondition);
					return true;
				case 2:
				case 0:
					return false;
				case -1:
				case 1:
					break;
			}
		}
		add(aCondition);
		return true;
	}

	public boolean addCondition(EMMCondition aCondition)
	{
		
		if (indexOf(aCondition) == -1) //already present?
		{
			add(aCondition);
			return true;
		}
		else
			return false;
	}

	private boolean findCondition(EMMCondition theCondition)
	{
		for (EMMCondition aCondition : this)
			if (theCondition.equals(aCondition))
				return true;
		return false;
	}

	// throws NullPointerException if theCondition is null.
	@Override
	public int compareTo(ConditionList theConditionList)
	{
		if (this == theConditionList)
			return 0;

		else if (this.size() < theConditionList.size())
			return -1;
		else if (this.size() > theConditionList.size())
			return 1;

		for (int i = 0, j = size(); i < j; ++i)
		{
			int aTest = this.get(i).compareTo(theConditionList.get(i));
			if (aTest != 0)
				return aTest;
		}

		return 0;
	}

	//this method computes logical equivalence. This means that the actual number of conditions or the order may differ.
	//Just as long as it effectively selects the same subgroup, no matter what the database is.
	//This method currently doesn't consider equivalence of the type a<10&a<20 vs. a<10 etc.
	@Override
	public boolean equals(Object theObject)
	{
		if (theObject == null || (theObject.getClass() != this.getClass()))
			return false;
		ConditionList aCL = (ConditionList) theObject;

		//check in one direction
		for (EMMCondition aCondition : aCL)
			if (!findCondition(aCondition))
				return false;

		//check in the other direction
		for (EMMCondition aCondition : this)
			if (!aCL.findCondition(aCondition))
				return false;

		return true;
	}

	@Override
	public String toString()
	{
		StringBuilder aResult = new StringBuilder(size() * 25);
		for(Condition aCondition : this)
		{
			aResult.append(aCondition);
			aResult.append(" && ");
		}
		if (size() == 0)
			return "empty";
		else
			return aResult.substring(0, aResult.length() - 4);
	}
}
