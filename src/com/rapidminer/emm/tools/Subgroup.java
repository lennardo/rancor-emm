package com.rapidminer.emm.tools;

import java.util.BitSet;

import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.AttributeValueFilter;
import com.rapidminer.example.set.Condition;

public class Subgroup implements Comparable<Subgroup>{

	
	BitSet itsMembers;
	
	private double measureValue, pValue,correlationValue, complementCorrelationValue;

	private boolean measureValueIsSet = false;
	private boolean pValueIsSet = false;
	private int coverage = 1000;
	private ExampleSet parent;
		
	ConditionList conditions;
	
	
	public Subgroup(BitSet theMembers, ConditionList theConditionList, double measureValue, double pValue){
		
		itsMembers = theMembers;
		if(theConditionList == null){
			conditions = new ConditionList();
		}else{
			conditions = theConditionList;			
		}
		this.measureValue = measureValue;
		this.pValue = pValue;
		
	}
	
	public Subgroup(ConditionList theConditionList){
		if(theConditionList == null){
			conditions = new ConditionList();
		}else{
			conditions = theConditionList;			
		}

	}
	public Subgroup(BitSet theMembers, ConditionList theConditionList){
		itsMembers = theMembers;
		conditions = theConditionList;
	}
	
	//TODO parent has to be set
	public Condition getConditionsAsSingleCondition(){
		String parameterString = conditions.toString(); 
		if(conditions.equals("empty")){
			return null;
		}else{
			
			return new AttributeValueFilter(parent, parameterString);
		}
	}
	
	
	public void setCoverage(int coverage){
		this.coverage = coverage;
	}
	public int getCoverage(){
		return this.coverage;
	}
	//maybe change to deep-copy, though shouldn't be necessary since conditions are not changed once created 
	public ConditionList getConditions(){
			return conditions.copy();	
	}

	public int getDescriptionLength(){
		return conditions.size();
	}
	public double getPValue(){
		return pValue;
	}
	
	public void setPValue(double theValue){
		pValue = theValue;
	}
	
	public void setMeasureValue(double theValue){
		measureValue = theValue;
	}
	
	public double getMeasureValue(){
		return measureValue;
	}
	
	public BitSet getMembers(){
		return (BitSet)itsMembers.clone();
	}
	
	public String conditionsToString(){
			return conditions.toString();
	}
	
	public double getCorrelationValue() {
		return correlationValue;
	}

	public void setCorrelationValue(double correlationValue) {
		this.correlationValue = correlationValue;
	}

	public double getComplementCorrelationValue() {
		return complementCorrelationValue;
	}

	public void setComplementCorrelationValue(double complementCorrelationValue) {
		this.complementCorrelationValue = complementCorrelationValue;
	}

	@Override//TODO compare conditionlist and coverage
	public int compareTo(Subgroup aSubgroup) {
		if(this == aSubgroup){
			return 0;
		}else if(this.getMeasureValue() < aSubgroup.getMeasureValue()){
			return -1;
		}else if(this.getMeasureValue() > aSubgroup.getMeasureValue()){
			return 1;
		}else{
			return 0;			
		}
	}
	
	@Override
	public boolean equals(Object theObject){
		if (theObject == null || (theObject.getClass() != this.getClass()))
			return false;
		
		ConditionList conditionsToCompare = ((Subgroup)theObject).getConditions();
		if(this.conditions.size() == conditionsToCompare.size()){
			
			for(int i =0; i< this.conditions.size();i++){
			
				if(this.conditions.get(i).compa(conditionsToCompare.get(i)) != 0){
					return false;
				}
			
			}
			
		}else{
			return false;
		}
		return true;
	}
	
}
