package com.rapidminer.emm.tools;

import java.io.Serializable;
import java.util.Comparator;

import com.rapidminer.example.Attribute;
import com.rapidminer.example.set.AttributeValueFilterSingleCondition;

public class EMMCondition extends AttributeValueFilterSingleCondition implements Comparable<EMMCondition>{
	
	private static final long serialVersionUID = -8388210903977283085L;

	private Attribute itsAttribute;
	private int itsComparisonType;
	private String itsValue;
	private double fuzzyBear;
	private boolean fuzzyValueSet = false;

	
	

	/**rapidminer code**----think about moving somewhere else...fuzzyValue needs to be only set once/
	/* Comparator for doubles using fuzz factor. */
	static class FuzzyComp implements Comparator<Double>, Serializable {

		private static final long serialVersionUID = -7752907616633799595L;
		
		private double fuzz; // comparison fuzz factor

		/* Constructor */
		FuzzyComp(double f) {
			fuzz = Math.abs(f);
		}

		public int compare(Double x, Double y) {
			return (x > y + fuzz) ? 1 : ((x < y - fuzz) ? -1 : 0);
		}
	}
	
	/**----**/
	
	public EMMCondition(Attribute attribute, int comparisonType, String value) {
		super(attribute, comparisonType, value);

		itsAttribute = attribute;
		itsComparisonType = comparisonType;
		itsValue = value;
	}
	
	public EMMCondition(Attribute attribute, int comparisonType, String value, double f) {
		super(attribute, comparisonType, value);
		fuzzyBear=f;
		fuzzyValueSet = true;
		itsAttribute = attribute;
		itsComparisonType = comparisonType;
		itsValue = value;
	}

	//TODO check again
	@Override
	public int compareTo(EMMCondition o) {
		
		
		 if(this == o){
			return 0;
		}else if(this.getAttribute().getTableIndex() < o.getAttribute().getTableIndex()){
            return -1;
		}else if(this.getAttribute().getTableIndex() > o.getAttribute().getTableIndex()){
			return 1;
		}else if(this.getAttribute() == o.getAttribute()){
			if(this.getComparisonType() < o.getComparisonType()){
				return -1;
			}else if(this.getComparisonType() > o.getComparisonType()){
				return 1;
			}else{
				int aCompare = this.getValue().compareTo(o.getValue());
				if(this.getComparisonType()==0){
					return (aCompare < 0 ? -1 : 0);
				}else if(this.getComparisonType()==1){ //TODO somehow mark older condition for removal, if smaller/bigger
					return (aCompare > 0 ? 1 : 0);
				}else{
					return (aCompare < 0 ? -1 : aCompare > 0 ? 1 : 0);
				}
				//return this.getValue().compareTo(o.getValue());
			}
		}
		return 0;
	}
	/*
	 * 2 = compared condition is weaker
	 * -2 = replace with compared condition
	 * -1 = not equal
	 * 0= conditions are equal
	 * -1,1= not equal
	 */
	public int compa(EMMCondition o){
		if(this.getAttribute() == o.getAttribute()){
			
			if(this.getComparisonType() == o.getComparisonType()){
				if(o.getAttribute().isNumerical()){
					
					int test ;
					if(fuzzyValueSet){
						FuzzyComp fc = new FuzzyComp(fuzzyBear);
						test = fc.compare(Double.valueOf(this.getValue()), Double.valueOf(this.getValue()));
					}else{
						test= Double.valueOf(this.getValue()).compareTo(Double.valueOf(this.getValue()));						
					}

					if(this.getComparisonType() ==0){
						return test < 0?  2 : (test > 0 ? -2 : 0);
					}else{
						return test < 0? -2 : (test > 0 ?  2 : 0);
					}		
				}else{//Nominal
					return this.getValue().equals(o.getValue()) ? 0 : 1;
				}
			}else{
				return this.getComparisonType() < o.getComparisonType() ? -1 : 1;
			}
			
		}else{
			return this.getAttribute().getTableIndex() < o.getAttribute().getTableIndex() ? -1: 1;
		}

	}
	
	@Override
	public boolean equals(Object theObject){
		if (theObject == null || (theObject.getClass() != this.getClass()))
			return false;
		
		if(this.compa((EMMCondition)theObject)==0){
				return true;
		}else{
			return false;
		}

	}

	public Attribute getAttribute(){
		return itsAttribute;
	}
	
	public int getComparisonType(){
		return itsComparisonType;
	}

	public String getValue(){
		return itsValue;
	}

	//TODO new String necessary?
	public EMMCondition copy() {
		EMMCondition aCopy = new EMMCondition(itsAttribute, itsComparisonType,new String( itsValue));
		return aCopy;
	}



}
