package com.rapidminer.operator;

import com.rapidminer.emm.qualityMeasures.QualityMeasure;
import com.rapidminer.emm.tools.RefinementOperator;
import com.rapidminer.example.ExampleSet;

public class SearchParameters {
	
	ExampleSet completeData; 
	QualityMeasure aQM;
	RefinementOperator aRefOp; 
	int noResults; 
	int beamWidth;
	int depth;

}
