package com.rapidminer.operator;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;

import com.rapidminer.emm.qualityMeasures.KendallCorrelation;
import com.rapidminer.emm.qualityMeasures.PearsonCorrelation;
import com.rapidminer.emm.qualityMeasures.QualityMeasure;
import com.rapidminer.emm.qualityMeasures.SpearmanCorrelation;
import com.rapidminer.emm.tools.EMMResult;
import com.rapidminer.emm.tools.LogWriter;
import com.rapidminer.emm.tools.RefinementOperator;
import com.rapidminer.emm.tools.SimpleRefinement;
import com.rapidminer.emm.tools.Subgroup;
import com.rapidminer.example.Attribute;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.AttributeValueFilter;
import com.rapidminer.example.set.ConditionedExampleSet;
import com.rapidminer.example.set.NoMissingAttributesCondition;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorDescription;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.ports.InputPort;
import com.rapidminer.operator.ports.OutputPort;
import com.rapidminer.parameter.ParameterType;
import com.rapidminer.parameter.ParameterTypeAttribute;
import com.rapidminer.parameter.ParameterTypeBoolean;
import com.rapidminer.parameter.ParameterTypeCategory;
import com.rapidminer.parameter.ParameterTypeDouble;
import com.rapidminer.parameter.ParameterTypeInt;
import com.rapidminer.parameter.UndefinedParameterError;
import com.rapidminer.parameter.conditions.BooleanParameterCondition;

public class RancorEMM extends Operator{
		
	/**------------------------Parameter names---------------------------------------**/
	private static final String TARGET_ATTRIBUTE_ONE 		= "first_target_attribute";
	private static final String TARGET_ATTRIBUTE_TWO 		= "second_target_attribute";
	private static final String CORRELATION_TYPE 	 		= "correlation_type";
	private static final String NUMBER_OF_RESULTS 	 		= "number_of_results";
	private static final String NUMBER_OF_BINS 		 		= "number_of_bins";
	private static final String BEAM_WIDTH 			 		= "beam_width";
	private static final String SEARCH_DEPTH 		 		= "search_depth";
	private static final String RESTRICT_COVERAGE 	 		= "Restrict_coverage?";
	private static final String MINIMUM_COVERAGE 	 		= "Minimum_coverage";
	private static final String MAXIMUM_COVERAGE 	 		= "Maximum_coverage";
	private static final String RESTRICT_MEASURE_VALUE 	 	= "Minimum_measure_value?";
	private static final String MINIMUM_MEASURE_VALUE 	 	= "Minimum_measure-value";
	private static final String RESTRICT_DESCRIPTION_LENGTH = "Restrict_number_of_conditions?";
	private static final String MINIMUM_CONDITION_COUNT     = "Minimum_condition_count";
	private static final String MAXIMUM_CONDITION_COUNT     = "Maximum_condition_count";
	
	/**-----------------------Ports-----------------------------------------------------**/
	private InputPort exampleSetInput = getInputPorts ().createPort ("exampleset", ExampleSet.class) ;
	private OutputPort originalOutput= getOutputPorts ().createPort("original");
	private OutputPort resultOverview = getOutputPorts().createPort("results");
	private OutputPort subSetOutput = getOutputPorts().createPort("Overview");
	
	List<OutputPort> outputs = new ArrayList<OutputPort>();
	
	/**-----------------------Misc------------------------------------------------------**/
	private static final String[] CORRELATION_TYPES = {"Pearson","Spearman","Kendall"};
	private LogWriter logger;
	
	
	
	

	public RancorEMM(OperatorDescription description) {
		super(description);
	}
	
	
	/**--Workhorse--**/
	
	/* (non-Javadoc)
	 * @see com.rapidminer.operator.Operator#doWork()
	 */
	@Override
	public void doWork() throws OperatorException{
		
		
		ExampleSet theDat = exampleSetInput.getData (ExampleSet.class) ;
		ExampleSet theData = new ConditionedExampleSet(theDat, new NoMissingAttributesCondition(theDat, null));

		
		//TODO encapsulate parameters in own object
		/**---------------------------------------------**/
		PriorityQueue<Subgroup> interestingSubgroups = topQEMM(theData,getQualityMeasure(getParameterAsString(CORRELATION_TYPE)),new SimpleRefinement(getParameterAsInt(NUMBER_OF_BINS),0.0),getParameterAsInt(NUMBER_OF_RESULTS),getParameterAsInt(BEAM_WIDTH),getParameterAsInt(SEARCH_DEPTH));
		
		//List of q-best found subgroups
		int nrOfResults = getParameterAsInt(NUMBER_OF_RESULTS);
		List<ExampleSet> results = new ArrayList<ExampleSet>(nrOfResults);
		List<Subgroup> resultSubgroups = new ArrayList<Subgroup>(nrOfResults);
		AttributeValueFilter condition;
		
		while(!interestingSubgroups.isEmpty()){
			Subgroup aSubgroup = interestingSubgroups.poll();
			resultSubgroups.add(aSubgroup);
			condition = new AttributeValueFilter(theData, aSubgroup.conditionsToString());
			results.add(new ConditionedExampleSet(theData, condition, false));
		}
		
		//getTransformer().addPassThroughRule(exampleSetInput, originalOutput);
		originalOutput.deliver(theData);
		subSetOutput.deliver(results.get(nrOfResults-1));
		resultOverview.deliver(new EMMResult(resultSubgroups,getParameterAsInt(NUMBER_OF_RESULTS),getParameterAsString(CORRELATION_TYPE)));
		logger.writeLineToLog(results.get(nrOfResults-1).getAttributes().toString());
		for(Example e: results.get(nrOfResults-1)){
			logger.writeLineToLog(e.toString(),theData.getName()+" "+getParameterAsString(CORRELATION_TYPE)+".sg");
		}

	}
	

	public PriorityQueue<Subgroup> topQEMM(ExampleSet completeData, QualityMeasure aQM, RefinementOperator aRefOp, int noResults, int beamWidth,int depth){
		
		//getting TargetAttributes
		Attribute targetOne = null;
		Attribute targetTwo = null;
		
		try {
			//TODO accesing a special attribute results in NullpointerException
			targetOne = completeData.getAttributes().get(getParameterAsString(TARGET_ATTRIBUTE_ONE)); 
			targetTwo = completeData.getAttributes().get(getParameterAsString(TARGET_ATTRIBUTE_TWO));
			if(!targetOne.isNumerical()){
				throw new IllegalArgumentException(targetOne.getName() + " is not numeric! "+"Targets must be numeric.");
			}
			if(!targetTwo.isNumerical()){
				throw new IllegalArgumentException(targetTwo.getName() + " is not numeric! "+"Targets must be numeric.");
			}
		} catch (UndefinedParameterError e2) {
			e2.printStackTrace();
		}
		
		//TODO use this
		ExampleSet targetData = extract(completeData, targetOne, targetTwo); //TODO keep in mind that two copies are created
		ExampleSet descriptiveData = exclude(completeData, targetOne, targetTwo);
		descriptiveData.getAttributes().clearSpecial(); //TODO necessary?
		
		PriorityQueue<Subgroup> results = new PriorityQueue<Subgroup>(noResults);
		PriorityQueue<Subgroup> beam;
		
		LinkedList<Subgroup> candidateQueue = new LinkedList<Subgroup>();
		candidateQueue.add(new Subgroup(null));
		
		for(int level=0; level < depth; level++ ){
			
			beam = new PriorityQueue<Subgroup>(beamWidth);
			
			while(!candidateQueue.isEmpty()){
				
				Subgroup seed = candidateQueue.poll();
				
				List<Subgroup> set = aRefOp.refine(seed, descriptiveData);
				//System.out.println("\nRefined "+seed.conditionsToString()+ " , "+set.size()+" refinements created");
				//System.out.println("Refinements: \n ------------------------------");
				/*for(Subgroup aNewSubgroup: set){
					System.out.println(aNewSubgroup.conditionsToString());
				}*/

				for(Subgroup aNewSubgroup: set){
					Double aMeasureValue = aQM.evaluate(aNewSubgroup,completeData,targetOne, targetTwo);
					Double aPValue = aQM.getPValue(aMeasureValue);
					aNewSubgroup.setCorrelationValue(aMeasureValue);
					aNewSubgroup.setPValue(aQM.getPValue(aMeasureValue));
					aNewSubgroup.setMeasureValue(1-aQM.getPValue(aMeasureValue));
					
					//check novelty, constraints and measurement values

					if(!results.contains(aNewSubgroup) && fulfilsConstraints(aNewSubgroup) && !aMeasureValue.isNaN() && !aPValue.isNaN()){	
						results.add(aNewSubgroup);
					}
					
					try {
						if(results.size() > getParameterAsInt(NUMBER_OF_RESULTS)){
							results.poll();
						}
					} catch (UndefinedParameterError e1) {
						e1.printStackTrace();
					}
					
					beam.add(aNewSubgroup);
					try {
						if(beam.size() > getParameterAsInt(BEAM_WIDTH)){
							beam.poll();
						}
					} catch (UndefinedParameterError e) {
						e.printStackTrace();
					}
					
				}
			}
				
			while(!beam.isEmpty()){				
				candidateQueue.add(beam.poll());		
			}	
		}	
		return results;
	}

	//TODO this should be done in a cleaner way (checking if constraints are set is only necessarry once)
	private boolean fulfilsConstraints(Subgroup aSubgroup) {
		
		//get constraints
		boolean restrictCoverage = getParameterAsBoolean(RESTRICT_COVERAGE);
		boolean restrictMeasureValue = getParameterAsBoolean(RESTRICT_MEASURE_VALUE);
		boolean restrictDescriptionLength = getParameterAsBoolean(RESTRICT_DESCRIPTION_LENGTH);
		
		int minCoverage=0;
		int maxCoverage=Integer.MAX_VALUE;
		
		double minMeasureValue=0.0;
		
		int minDescriptionLength = 1;
		int maxDescriptionLength= Integer.MAX_VALUE;
		
		if(restrictCoverage){
			try {
				minCoverage = getParameterAsInt(MINIMUM_COVERAGE);
			} catch (UndefinedParameterError e) {
				throw new IllegalArgumentException("No value set for minimum coverage!");
			}
			try {
				maxCoverage =getParameterAsInt(MAXIMUM_COVERAGE);
			} catch (UndefinedParameterError e) {
				throw new IllegalArgumentException("No value set for maximum coverage!");

			}
			if(aSubgroup.getCoverage() < minCoverage || aSubgroup.getCoverage() > maxCoverage){
				return false;
			}
		}
		
		if(restrictMeasureValue){
			try {
				minMeasureValue = getParameterAsDouble(MINIMUM_MEASURE_VALUE);
			} catch (UndefinedParameterError e) {
				throw new IllegalArgumentException("No value set for minimum P-value!");
			}
			
			if(aSubgroup.getMeasureValue() < minMeasureValue){
				return false;
			}
		}
		
		if(restrictDescriptionLength){
			try {
				minDescriptionLength = getParameterAsInt(MINIMUM_CONDITION_COUNT);
			} catch (UndefinedParameterError e) {
				throw new IllegalArgumentException("No value set for minimum description length!");

			}
			try {
				maxDescriptionLength = getParameterAsInt(MAXIMUM_CONDITION_COUNT);
			} catch (UndefinedParameterError e) {
				throw new IllegalArgumentException("No value set for maximum description length!");

			}
			
			if(aSubgroup.getDescriptionLength() < minDescriptionLength || aSubgroup.getDescriptionLength() > maxDescriptionLength){
				return false;
			}
		}
		
		
		return true;
	}

	private QualityMeasure getQualityMeasure(String name){		
  
  		switch(name){
			case "Pearson":
				return new PearsonCorrelation();
			case "Spearman":
				return new SpearmanCorrelation();
			case "Kendall":
				return new KendallCorrelation();
			default:
				return new PearsonCorrelation();
		}

	}
	
	//ParameterList
	@Override
	public List<ParameterType> getParameterTypes() {
		
		List<ParameterType> types = new LinkedList<ParameterType>();
		
		ParameterType type = new ParameterTypeAttribute(TARGET_ATTRIBUTE_ONE, "First attribute which should be a target.", exampleSetInput);
		type.setExpert(false);
		types.add(type);
		
		type = new ParameterTypeAttribute(TARGET_ATTRIBUTE_TWO, "Second attribute which should be a target.",exampleSetInput);
		type.setExpert(false);
		types.add(type);
		
		type = new ParameterTypeCategory(CORRELATION_TYPE, "The coefficient used as a quality measure", CORRELATION_TYPES, 0);
		type.setExpert(false);
		types.add(type);

		type = new ParameterTypeInt(NUMBER_OF_RESULTS, "Number of Subgroup results", 1, Integer.MAX_VALUE, 10);
		type.setExpert(false);
		types.add(type);
		
		type = new ParameterTypeInt(BEAM_WIDTH, "Width of the beam", 1, Integer.MAX_VALUE, 10);
		type.setExpert(true);
		types.add(type);
		
		type = new ParameterTypeInt(SEARCH_DEPTH, "The search depth", 1, Integer.MAX_VALUE, 3);
		type.setExpert(true);
		types.add(type);
		
		type = new ParameterTypeInt(NUMBER_OF_BINS,"Number of bins used for refining numerical attributes",1,Integer.MAX_VALUE,10);
		type.setExpert(true);
		types.add(type);
		
		type = new ParameterTypeBoolean(RESTRICT_COVERAGE,"If checked, a minimum and maximum coverage value can be set",false,false);
		types.add(type);
		
		type = new ParameterTypeInt(MINIMUM_COVERAGE,"This parameter defines the minimum coverage a subgroup must have", 1,Integer.MAX_VALUE,5);
		type.registerDependencyCondition(new BooleanParameterCondition(this,RESTRICT_COVERAGE, true, true));
		types.add(type);
		
		type = new ParameterTypeInt(MAXIMUM_COVERAGE,"This parameter defines the maximum coverage a subgroup can have", 1,Integer.MAX_VALUE,100);
		type.registerDependencyCondition(new BooleanParameterCondition(this,RESTRICT_COVERAGE, true, true));
		types.add(type);
		
		type = new ParameterTypeBoolean(RESTRICT_MEASURE_VALUE,"If checked, a minimum measure value can be set",false,false);
		type.setExpert(true);
		types.add(type);
		
		type = new ParameterTypeDouble(MINIMUM_MEASURE_VALUE,"This parameter defines the minimum measure value a subgroup must have", 0.0,1.0,0.8);
		type.registerDependencyCondition(new BooleanParameterCondition(this,RESTRICT_MEASURE_VALUE, true, true));
		type.setExpert(true);
		types.add(type);
		
		type = new ParameterTypeBoolean(RESTRICT_DESCRIPTION_LENGTH,"If checked, a minimum and maximum length for a subgroup description can be set", false, false);
		type.setExpert(true);
		types.add(type);
		
		type = new ParameterTypeInt(MINIMUM_CONDITION_COUNT,"This parameter defines the minimum description length a subgroup must have", 1,Integer.MAX_VALUE,1);
		type.registerDependencyCondition(new BooleanParameterCondition(this,RESTRICT_DESCRIPTION_LENGTH, true, true));
		type.setExpert(true);
		types.add(type);
		
		type = new ParameterTypeInt(MAXIMUM_CONDITION_COUNT,"This parameter defines the maximum description length a subgroup can have", 1,Integer.MAX_VALUE,6);
		type.registerDependencyCondition(new BooleanParameterCondition(this,RESTRICT_DESCRIPTION_LENGTH, true, true));
		type.setExpert(true);
		types.add(type); 

		
		types.addAll(super.getParameterTypes());
		return types;
	}

	/**------helper functions----**/

	/* 
	 * Extracts an example set containing just the two specified
	 * attributes and no missing values.
	 * 
	 * @param eSet the source example set
	 * @param a the first attribute to extract
	 * @param b the second attribute to extract
	 * @return the reduced example set
	 */
	private ExampleSet extract(ExampleSet eSet, Attribute a, Attribute b) {
		ExampleSet e = (ExampleSet) eSet.clone();
		e.getAttributes().clearRegular();
		e.getAttributes().clearSpecial();
		e.getAttributes().addRegular(a);
		e.getAttributes().addRegular(b);
		return new ConditionedExampleSet(e, new NoMissingAttributesCondition(e, null));
	}
	
	/* 
	 * Creates an example set excluding  the two specified
	 * attributes.
	 * 
	 * @param eSet the source example set
	 * @param a the first attribute to exclude
	 * @param b the second attribute to exclude
	 * @return the reduced example set
	 */
	
	private ExampleSet exclude(ExampleSet eSet, Attribute a, Attribute b){
		ExampleSet e = (ExampleSet) eSet.clone();
		e.getAttributes().remove(a);
		e.getAttributes().remove(b);
		return e;
	}
	
	/**----Graveyard---**/
	
	/* Comparator for doubles using fuzz factor. */
	static class FuzzyComp implements Comparator<Double>, Serializable {

		private static final long serialVersionUID = -7752907616633799595L;
		
		private double fuzz; // comparison fuzz factor

		/* Constructor */
		FuzzyComp(double f) {
			fuzz = Math.abs(f);
		}

		public int compare(Double x, Double y) {
			return (x > y + fuzz) ? 1 : ((x < y - fuzz) ? -1 : 0);
		}
	}
}
