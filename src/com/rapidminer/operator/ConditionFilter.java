package com.rapidminer.operator;

import java.util.BitSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.rapidminer.example.Attributes;
import com.rapidminer.example.Example;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.example.set.AbstractExampleSet;
import com.rapidminer.example.set.Condition;
import com.rapidminer.example.set.MappedExampleReader;
import com.rapidminer.example.table.ExampleTable;

public class ConditionFilter extends AbstractExampleSet{

	
	private static final long serialVersionUID = -2864168655564035602L;

	private ExampleSet parent; 
	private int[] mapping;
	
	
	public ConditionFilter(ExampleSet theData, Condition condition, boolean inverted){
		this.mapping = calculateMapping(condition, inverted);
		parent = theData;
	}
	public ConditionFilter(ExampleSet theData, Condition condition){
		this(theData,condition, false);
	}

	@Override
	public ExampleTable getExampleTable() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Example getExample(int index) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override

	public Iterator<Example> iterator() {
		return new MappedExampleReader(parent.iterator(), this.mapping);
	}

	
    
	private int[] calculateMapping(Condition condition, boolean inverted) {
	List<Integer> indices = new LinkedList<Integer>();
	
	// create mapping
	int exampleCounter = 0;
	for (Example example : parent) {
        if (!inverted) {
            if (condition.conditionOk(example))
                indices.add(exampleCounter);
        } else {
            if (!condition.conditionOk(example))
                indices.add(exampleCounter);                
        }
		exampleCounter++;
	}
	
	int[] mapping = new int[indices.size()];
	int m = 0;
	for (int index : indices) {
		mapping[m++] = index;
	}
	return mapping;
	}
	
	public int size() {
		return mapping.length;
	}

    public Attributes getAttributes() {
    	return parent.getAttributes();
    }

}
