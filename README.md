RapidMiner Exceptional Model Mining Extension 
=============================

Link to paper: https://link.springer.com/article/10.1007/s10115-016-0979-z

Building the Extension. 

### Getting started
1. Checkout [RapidMiner](https://github.com/rapidminer/rapidminer) (e.g. to _~/git/rapidminer_).

2. Build and install your extension by executing the Ant target "install" 

3. Start RapidMiner and check whether your extension has been loaded